from setuptools import setup
setup(
    name='trafficlight',
    version='0.1',
    description='Install packages traffic light recognition',
    url='https://mekongsmartcam.vn',
    author='HuynhLong',
    author_email='longh.tgg@gmail.com',
    license='MIT',
    install_requires=["python-resize-image==1.1.19"],
    packages=['trafficlight'],
    include_package_data=True,
    zip_safe=False
)