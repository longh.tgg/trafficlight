from PIL import Image
import numpy as np
import pickle
import os
from resizeimage import resizeimage


class Trafficlight(object):

    def __init__(self):
        this_dir, this_filename = os.path.split(__file__)
        DATA_PATH = os.path.join(this_dir, "traffic.pkl")
        self.__model = pickle.load(open(DATA_PATH, 'rb'))

    def predict(self, frame, condition):
        width, height = frame.size
        x = int(condition[0] * width)
        y = int(condition[1] * height)
        w = int(condition[2] * width)
        h = int(condition[3] * height)

        traffic_light = frame.crop((x, y, x + w, y + h))
        resize_traffic = resizeimage.resize_cover(
            traffic_light, [20, 50], validate=False)
        resize_traffic = resize_traffic.convert('RGB')
        resize_traffic = np.array(resize_traffic)
        resize_traffic = resize_traffic.reshape(1, -1)
        result = self.__model.predict(resize_traffic)[0]
        # Result = 2 is Yellow , result = 1 is red result =0 is green
        return result
